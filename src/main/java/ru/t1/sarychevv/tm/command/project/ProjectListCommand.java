package ru.t1.sarychevv.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.enumerated.ProjectSort;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Show list projects.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        @NotNull final String userId = getUserId();
        @NotNull final List<Project> projects;
        if (sort == null) projects = getProjectService().findAll(userId);
        else projects = getProjectService().findAll(userId, sort.getComparator());
        int index = 0;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getId() + ": " + project.getName() +
                    " " + project.getDescription() + " " + Status.toName(project.getStatus()));
            index++;
        }
    }

}

package ru.t1.sarychevv.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Vladimir Sarychev");
        System.out.println("e-mail: vsarychev@t1-consulting.ru");
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show about program.";
    }

}

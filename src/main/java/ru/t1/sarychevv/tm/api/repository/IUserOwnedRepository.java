package ru.t1.sarychevv.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    int getSize(@NotNull String userId);

    void removeAll(@NotNull String userId);

    @Nullable
    M removeOne(@NotNull String userId, @NotNull M model);

    @Nullable
    M removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    M add(@NotNull String userId, @NotNull M model);

}

package ru.t1.sarychevv.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.IRepository;
import ru.t1.sarychevv.tm.api.service.IService;
import ru.t1.sarychevv.tm.exception.field.IdEmptyException;
import ru.t1.sarychevv.tm.exception.field.IndexIncorrectException;
import ru.t1.sarychevv.tm.exception.field.ModelEmptyException;
import ru.t1.sarychevv.tm.exception.field.ModelIdEmptyException;
import ru.t1.sarychevv.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    protected AbstractService(@NotNull R repository) {
        this.repository = repository;
    }

    @Override
    public void add(@Nullable final M model) {
        if (model == null) throw new ModelEmptyException();
        repository.add(model);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ModelIdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final M model) {
        if (model == null) throw new ModelEmptyException();
        repository.removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeOneById(id);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeOneByIndex(index);
    }

    @NotNull
    @Override
    public Integer getSize() {
        return repository.getSize();
    }

    @NotNull
    @Override
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

}
